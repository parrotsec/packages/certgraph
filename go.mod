module github.com/lanrat/certgraph

require (
	github.com/lib/pq v1.10.5
	github.com/weppos/publicsuffix-go v0.15.0
	golang.org/x/net v0.0.0-20220425223048-2871e0cb64e4 // indirect
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
)

go 1.16
